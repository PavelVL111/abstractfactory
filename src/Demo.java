import Factory.FactoryMac;
import Factory.FactoryWin;

public class Demo {
    public static void main(String[] args) {
        String nameOfOs = "win";
        Application application;
        if(nameOfOs.equals("win")){
            application = new Application(new FactoryWin());
            application.paint();
        }
        nameOfOs = "mac";
        if(nameOfOs.equals("mac")){
            application = new Application(new FactoryMac());
            application.paint();
        }
    }
}
