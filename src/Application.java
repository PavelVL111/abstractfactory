import Element.Button.Button;
import Element.Checkbox.Checkbox;
import Factory.FactoryElement;

public class Application {
    Checkbox checkbox;
    Button button;

    public Application(FactoryElement factoryElement){
        this.button = factoryElement.createButton();
        this.checkbox = factoryElement.createCheckbox();
    }

    public void paint(){
        checkbox.paint();
        button.paint();
    }
}
