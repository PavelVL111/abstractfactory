package Factory;

import Element.Button.Button;
import Element.Button.ButtonWin;
import Element.Checkbox.Checkbox;
import Element.Checkbox.CheckboxWin;

public class FactoryWin extends FactoryElement {
    @Override
    public Button createButton() {
        return new ButtonWin();
    }

    @Override
    public Checkbox createCheckbox() {
        return new CheckboxWin();
    }
}
