package Factory;

import Element.Button.Button;
import Element.Button.ButtonMac;
import Element.Checkbox.Checkbox;
import Element.Checkbox.CheckboxMac;

public class FactoryMac extends FactoryElement{

    @Override
    public Button createButton() {
        return new ButtonMac();
    }

    @Override
    public Checkbox createCheckbox() {
        return new CheckboxMac();
    }
}
