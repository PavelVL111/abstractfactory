package Factory;

import Element.Button.Button;
import Element.Checkbox.Checkbox;

public abstract class FactoryElement {

    public abstract Button createButton();

    public abstract Checkbox createCheckbox();

}
